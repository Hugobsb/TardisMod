package net.tardis.mod.registries;

import java.util.function.Supplier;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.protocols.AntiGravProtocol;
import net.tardis.mod.protocols.ForcefieldProtocol;
import net.tardis.mod.protocols.LifeScanProtocol;
import net.tardis.mod.protocols.Protocol;
import net.tardis.mod.protocols.ToggleAlarmProtocol;

public class ProtocolRegistry {
	
    public static final DeferredRegister<Protocol> PROTOCOLS = DeferredRegister.create(Protocol.class, Tardis.MODID);
	
	public static Supplier<IForgeRegistry<Protocol>> PROTOCOL_REGISTRY = PROTOCOLS.makeRegistry("protocol", () -> new RegistryBuilder<Protocol>().setMaxID(Integer.MAX_VALUE - 1));
	
	public static final RegistryObject<Protocol> LIFE_SCAN = PROTOCOLS.register("life_scan", () -> new LifeScanProtocol());
	public static final RegistryObject<Protocol> TOGGLE_ALARM = PROTOCOLS.register("toggle_alarm", () -> new ToggleAlarmProtocol());
	public static final RegistryObject<Protocol> ANTI_GRAV = PROTOCOLS.register("anti_grav", () -> new AntiGravProtocol());
	public static final RegistryObject<Protocol> FORCEFIELD = PROTOCOLS.register("forcefield", () -> new ForcefieldProtocol());
}
