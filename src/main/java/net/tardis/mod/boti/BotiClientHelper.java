package net.tardis.mod.boti;

/** Helper class that runs client side only code, prevents classloading, ClassNotFound or NoSuchMethod errors on dedicated servers*/
public class BotiClientHelper {
    
    public static int packLight(int blockLightIn, int skyLightIn) {
        return blockLightIn << 4 | skyLightIn << 20;
     }

}
