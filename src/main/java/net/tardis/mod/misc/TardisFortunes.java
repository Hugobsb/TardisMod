package net.tardis.mod.misc;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;

public class TardisFortunes {
	
    public static List<String> FORTUNES = new ArrayList<>();
	
    /** Call on the logical client only*/
	public static void readFortunes() {
        try {
        	FORTUNES.clear();
            List<String> list = new ArrayList<String>();

            InputStream is = Minecraft.getInstance().getResourceManager().getResource(new ResourceLocation(Tardis.MODID, "fortunes.json")).getInputStream();
            JsonArray array = new JsonParser().parse(new JsonReader(new InputStreamReader(is))).getAsJsonArray();
            for (JsonElement ele : array) {
                list.add(ele.getAsString());
            }
            FORTUNES.addAll(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
