package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.particles.ParticleType;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.particles.TParticleTypes;

public class TardisParticleGen implements IDataProvider {

	private DataGenerator gen;
	
	public TardisParticleGen(DataGenerator gen) {
		this.gen = gen;
	}
	
	@Override
	public void act(DirectoryCache cache) throws IOException {
		
		Path base = gen.getOutputFolder();
		
		makeParticle(TParticleTypes.BUBBLE.get(), Helper.createRL("bubble/bubble_redux"), 75, cache, base);
		
	}
	
	public void makeParticle(ParticleType<?> type, ResourceLocation textureName, int count, DirectoryCache cache, Path base) throws IOException {
		IDataProvider.save(DataGen.GSON, cache, this.createParticle(textureName, count), getPath(base, type.getRegistryName()));
	}
	
	public static Path getPath(Path base, ResourceLocation name) {
		return base.resolve("assets/" + name.getNamespace() + "/particle/" + name.getPath() + ".json");
	}
	
	public JsonObject createParticle(ResourceLocation baseName, int max){
		JsonObject root = new JsonObject();
		
		JsonArray textures = new JsonArray();
		
		for(int i = 0; i < max; ++i) {
			textures.add(baseName.getNamespace() + ":" + baseName.getPath() + i);
		}
		
		root.add("textures", textures);
		
		return root;
	}

	@Override
	public String getName() {
		return "Tardis Particle Gen";
	}

}
