package net.tardis.mod.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.Pose;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.util.*;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
/** Entity to allow entities to teleport to its stored interior world*/
public class TardisBackdoorEntity extends Entity{
    
    private RegistryKey<World> interiorWorldKey;
    private int activatedTicks = 0;

    public TardisBackdoorEntity(EntityType<?> entityTypeIn, World worldIn) {
        super(entityTypeIn, worldIn);
    }
    
    public TardisBackdoorEntity(World worldIn) {
        super(TEntities.TARDIS_BACKDOOR.get(), worldIn);
    }

    @Override
    protected void registerData() {
    }

    @Override
    protected void readAdditional(CompoundNBT compound) {
        this.interiorWorldKey = WorldHelper.getWorldKeyFromRL(new ResourceLocation(compound.getString("interior_world_key")));
    }

    @Override
    protected void writeAdditional(CompoundNBT compound) {
        if (this.interiorWorldKey != null)
            compound.putString("interior_world_key", this.interiorWorldKey.getLocation().toString());
    }

    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    public void moveThroughPortal(Entity entity) {
        if (!this.world.isRemote() && entity.getType() != TEntities.TARDIS_BACKDOOR.get()) {
            if (this.interiorWorldKey != null) {
                if(this.activatedTicks <= 0)
                    this.activatedTicks = 60;

                world.getServer().enqueue(new TickDelayedTask(1, () -> {
                    WorldHelper.teleportEntities(entity, world.getServer().getWorld(interiorWorldKey), TardisHelper.TARDIS_POS.up(), entity.rotationYaw, entity.rotationPitch);
                    this.world.playSound(null, this.getPosition(), SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, SoundCategory.PLAYERS, 1F, 1F);
                    entity.world.playSound(null, entity.getPosition(), SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, SoundCategory.PLAYERS, 1F, 1F);
                }));

            }
        }
    }

    @Override
    public void tick() {
        super.tick();

        for(Entity e : world.getEntitiesWithinAABB(Entity.class, this.getBoundingBox())){
            if(e.isAlive() && e != this)
                moveThroughPortal(e);
        }

        if(this.activatedTicks > 0){
            --activatedTicks;
            if(activatedTicks == 0)
                this.remove();
        }

    }

    @Override
	public EntitySize getSize(Pose poseIn) {
		return super.getSize(poseIn);
	}

	public RegistryKey<World> getInteriorWorldKey(){
        return interiorWorldKey;
    }
    
    public void setInteriorWorldKey(RegistryKey<World> interiorWorldKey) {
        this.interiorWorldKey = interiorWorldKey;
    }

    @Override
    public boolean isImmuneToFire() {
        return true;
    }

    @Override
    public boolean isInvulnerableTo(DamageSource source) {
        return super.isInvulnerableTo(source);
    }

    @Override
    public boolean isImmuneToExplosions() {
        return true;
    }

}
