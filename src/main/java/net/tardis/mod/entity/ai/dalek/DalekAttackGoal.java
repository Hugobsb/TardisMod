package net.tardis.mod.entity.ai.dalek;

import java.util.EnumSet;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.player.PlayerEntity;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;
import net.tardis.mod.misc.AbstractWeapon;

public class DalekAttackGoal extends NearestAttackableTargetGoal<LivingEntity> {
	protected final Class<LivingEntity> targetClass;
    private final DalekEntity dalek;
    private int tickCounter;
    private double maxAttackDistance;
    private int seeTime;
    private boolean strafingClockwise;
    private boolean strafingBackwards;
    private int strafingTime = -1;
    protected final boolean shouldCheckSight;
    protected final boolean nearbyOnly;

    public DalekAttackGoal(DalekEntity dalek, Class<LivingEntity> targetClass, boolean shouldCheckSight, boolean nearbyOnly, double maxAttackDistance) {
        super(dalek, targetClass, shouldCheckSight, nearbyOnly);
        this.targetClass = targetClass;
        this.shouldCheckSight = shouldCheckSight;
        this.nearbyOnly = nearbyOnly;
    	this.dalek = dalek;
        this.setMutexFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK, Goal.Flag.TARGET));
        this.maxAttackDistance = maxAttackDistance;
    }

    @Override
    public void startExecuting() {
    	super.startExecuting();
        this.tickCounter = 0;
    }

    @Override
    public void resetTask() {
    	super.resetTask();
        this.seeTime = 0;
        this.tickCounter = 0;
        this.dalek.setAttackTarget(null);
        this.nearestTarget = null;
    }

    @Override
    public void tick() {
        if (!this.dalek.world.isRemote) {
            if (nearestTarget != null) {
            	this.dalek.setAttackTarget(this.nearestTarget);
                double distToTarget = this.dalek.getDistanceSq(nearestTarget.getPosX(), nearestTarget.getPosY(), nearestTarget.getPosZ());
                boolean canSeeTarget = this.dalek.getEntitySenses().canSee(nearestTarget);
                boolean targetSeen = this.seeTime > 0;
                
                /** Ranged strafe logic start*/
                if (canSeeTarget != targetSeen) {
                   this.seeTime = 0;
                }

                if (canSeeTarget) {
                   ++this.seeTime;
                } else {
                   --this.seeTime;
                }

                if (!(distToTarget > (double)this.maxAttackDistance) && this.seeTime >= 20) {
                   this.dalek.getNavigator().clearPath();
                   ++this.strafingTime;
                } else {
                   this.dalek.getNavigator().tryMoveToEntityLiving(nearestTarget, this.dalek.getDalekType().getMovementSpeed());
                   this.strafingTime = -1;
                }
                
                if (this.strafingTime >= 20) {
                    if ((double)this.dalek.getRNG().nextFloat() < 0.3D) {
                        this.strafingClockwise = !this.strafingClockwise;
                    }

                    if ((double)this.dalek.getRNG().nextFloat() < 0.3D) {
                        this.strafingBackwards = !this.strafingBackwards;
                    }

                    this.strafingTime = 0;
                }
                
                if (this.strafingTime > -1) {
                    if (distToTarget > (double)(this.maxAttackDistance * 0.75F)) {
                       this.strafingBackwards = false;
                    } else if (distToTarget < (double)(this.maxAttackDistance * 0.25F)) {
                       this.strafingBackwards = true;
                    }
                    this.dalek.getMoveHelper().strafe(this.strafingBackwards ? -0.5F : 0.5F, this.strafingClockwise ? 0.5F : -0.5F);
                    this.dalek.faceEntity(nearestTarget, 30.0F, 30.0F);
                } else {
                    this.dalek.getLookController().setLookPositionWithEntity(nearestTarget, 30.0F, 30.0F);
                }
                /** Ranged strafe logic end*/
                
                if (canSeeTarget) {
                	++this.tickCounter;
                    if (this.tickCounter >= getTicksUntilAttack()) {
                    	this.dalek.faceEntity(nearestTarget, 30.0F, 30.0F);
                        AbstractWeapon<DalekEntity> weapon = this.dalek.getDalekType().getWeapon();
                        if (weapon != null) {
                            if (weapon.useWeapon(dalek)) {
                                this.dalek.playSound(this.dalek.getDalekType().getFireSound(dalek), this.dalek.getSoundVolume(), this.dalek.getSoundPitch());
                                if (nearestTarget instanceof MobEntity) {
                                    MobEntity mob = (MobEntity)nearestTarget;
                                    mob.setAggroed(true);
                                }
                            }
                        }
                        this.tickCounter = 0;
                    }
                }
            }
        }
    }
    
    public double getTicksUntilAttack() {
        double attacksPerSecond = this.dalek.getDalekType().getAttacksPerSecond();
        double ticksTillAttack = (1.0 / attacksPerSecond) * 20;
        return ticksTillAttack;
    }

    @Override
    protected void findNearestTarget() {
        if (!(this.nearestTarget instanceof PlayerEntity)) {
           this.nearestTarget = this.dalek.world.getClosestEntity(this.targetClass, DalekEntity.DEFAULT_ENEMY_CONDITION.setCustomPredicate(this.dalek.getDalekType().getAttackPredicate()), this.dalek, this.dalek.getPosX(), this.dalek.getPosYEye(), this.dalek.getPosZ(), this.dalek.getTargetableArea(this.dalek.getTargetDistance()));
        } else {
           this.nearestTarget = this.dalek.world.getClosestPlayer(this.goalOwner, this.dalek.getTargetDistance());
        }

    }
}