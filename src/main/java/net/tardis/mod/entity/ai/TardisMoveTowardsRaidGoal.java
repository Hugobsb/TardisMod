package net.tardis.mod.entity.ai;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.entity.ai.goal.MoveTowardsRaidGoal;
import net.minecraft.entity.monster.AbstractRaiderEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.raid.Raid;
import net.minecraft.world.raid.RaidManager;

/** Modified version of MoveTowardsRaidGoal with parameters for movement speed
 * <br> Vanilla one is hardcoded to a particular speed which is too fast for our purposes*/
public class TardisMoveTowardsRaidGoal extends MoveTowardsRaidGoal<AbstractRaiderEntity>{
    protected final AbstractRaiderEntity raider;
    protected double speed;
    
	public TardisMoveTowardsRaidGoal(AbstractRaiderEntity raider, double speed) {
		super(raider);
		this.raider = raider;
		this.speed = speed;
	}
	
	@Override
	public void tick() {
	    if (this.raider.isRaidActive()) {
	        Raid raid = this.raider.getRaid();
	        if (this.raider.ticksExisted % 20 == 0) {
	           this.inviteNearbyPeersToRaid(raid);
	        }

	        if (!this.raider.hasPath()) {
	           Vector3d vector3d = RandomPositionGenerator.findRandomTargetBlockTowards(this.raider, 15, 4, Vector3d.copyCenteredHorizontally(raid.getCenter()));
	           if (vector3d != null) {
	              this.raider.getNavigator().tryMoveToXYZ(vector3d.x, vector3d.y, vector3d.z, this.speed);
	           }
	        }
	    }
	}

	protected void inviteNearbyPeersToRaid(Raid raid) {
        if (raid.isActive()) {
           Set<AbstractRaiderEntity> set = Sets.newHashSet();
           List<AbstractRaiderEntity> list = this.raider.world.getEntitiesWithinAABB(AbstractRaiderEntity.class, this.raider.getBoundingBox().grow(16.0D), (raider) -> {
              return !raider.isRaidActive() && RaidManager.canJoinRaid(raider, raid);
           });
           set.addAll(list);

           for(AbstractRaiderEntity abstractraiderentity : set) {
              raid.joinRaid(raid.getGroupsSpawned(), abstractraiderentity, (BlockPos)null, true);
           }
        }
    }

}
