package net.tardis.mod.entity.ai;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;

public class FlyingMovementGoal extends Goal {

	private float heightOffset = 0.5F;
    private int heightOffsetUpdateTime;
    private CreatureEntity creature;

    public FlyingMovementGoal(CreatureEntity creature) {
        this.creature = creature;
    }

    @Override
    public boolean shouldExecute() {
        return creature.getAttackTarget() != null && shouldFly(creature);
    }

    private boolean shouldFly(CreatureEntity creature) {
        if (creature instanceof DalekEntity) {
            DalekEntity dalek = (DalekEntity) creature;
            return dalek.getDalekType().canFly();
        }
        return true;
    }

    @Override
    public boolean shouldContinueExecuting() {
        return shouldExecute();
    }

    @Override
    public void tick() {
        super.tick();
        Vector3d vec3d = creature.getMotion();
        LivingEntity victim = creature.getAttackTarget();
        boolean victimFlag = victim != null && victim.getPosY() + (double) victim.getEyeHeight() > creature.getPosY() + (double) creature.getEyeHeight() + (double) this.heightOffset && creature.canAttack(victim);
        if (victimFlag) {
            --this.heightOffsetUpdateTime;
            if (this.heightOffsetUpdateTime <= 0) {
                this.heightOffsetUpdateTime = 100;
                this.heightOffset = 0.5F + (float) creature.world.rand.nextGaussian() * 3.0F;
            }

            creature.setMotion(creature.getMotion().add(0.0D, (0.3D - vec3d.y) * 0.3D, 0.0D));
            creature.isAirBorne = true;
            this.creature.getNavigator().tryMoveToEntityLiving(victim, creature.getAttributeValue(Attributes.FLYING_SPEED));

            if (creature.getEntitySenses().canSee(victim) && creature.getDistance(victim) < 32 && creature.getDistance(victim) > 5) {
                this.creature.getNavigator().tryMoveToEntityLiving(victim, creature.getAttributeValue(Attributes.FLYING_SPEED));
            }
        } else {
            heightOffset = 0.5F;
        }
    }

}
