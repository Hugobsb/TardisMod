package net.tardis.mod.artron;

import net.minecraft.item.ItemStack;

public interface IArtronItemStackBattery {

    /**
     * @param stack  - the itemstack we are targeting
     * @param amount - how much the source offers to the battery
     * @param simulate - if this is being simulated
     * @return how much charge was accepted from the source
     */
    float charge(ItemStack stack, float amount, boolean simulate);
    
    default float charge(ItemStack stack, float amount) {
    	return charge(stack, amount, false);
    }

    /**
     * @param stack  - the itemstack we are targeting
     * @param amount - how much the object tries to take from the battery
     * @param simulate - if this is being simulated
     * @return how much charge was taken from the battery
     */
    float discharge(ItemStack stack, float amount, boolean simulate);
    
    default float discharge(ItemStack stack, float amount) {
    	return discharge(stack, amount, false);
    }

    float getMaxCharge(ItemStack stack);

    /**
     * Convenience method to get the current charge of the itemstack
     *
     * @param stack
     * @return
     */
    float getCharge(ItemStack stack);
}
