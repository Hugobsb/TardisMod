package net.tardis.mod.traits;

import java.util.function.Function;
import java.util.function.Predicate;

import net.minecraftforge.registries.ForgeRegistryEntry;

public class TardisTraitType extends ForgeRegistryEntry<TardisTraitType>{

	Function<TardisTraitType, TardisTrait> supplier;
	Predicate<TardisTraitType> incompatibleTraits;
	
	public TardisTraitType(Function<TardisTraitType, TardisTrait> supplier, Predicate<TardisTraitType> test) {
		this.supplier = supplier;
		this.incompatibleTraits = test;
	}
	
	public TardisTrait create() {
		return this.supplier.apply(this);
	}
	
	public boolean isCompatible(TardisTraitType type) {
		return !this.incompatibleTraits.test(type);
	}

	public Class<? extends TardisTrait> getTraitClass() {
		return this.supplier.apply(this).getClass();
	}
	
}
