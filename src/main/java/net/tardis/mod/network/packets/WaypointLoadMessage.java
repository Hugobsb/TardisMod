package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.WaypointBankTile;

public class WaypointLoadMessage {

	public static final String TRANSLATION = "message.waypoint.loaded";
	BlockPos bank;
	int index = 0;
	
	public WaypointLoadMessage(BlockPos bank, int index) {
		this.index = index;
		this.bank = bank;
	}
	
	public static void encode(WaypointLoadMessage mes, PacketBuffer buf) {
		buf.writeBlockPos(mes.bank);	
		buf.writeInt(mes.index);
	}
	
	public static WaypointLoadMessage decode(PacketBuffer buf) {
		return new WaypointLoadMessage(buf.readBlockPos(), buf.readInt());
	}
	
	public static void handle(WaypointLoadMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			TardisHelper.getConsoleInWorld(cont.get().getSender().getServerWorld()).ifPresent(tile -> {
				TileEntity te = cont.get().getSender().world.getTileEntity(mes.bank);
				if(te instanceof WaypointBankTile) {
					SpaceTimeCoord coord = ((WaypointBankTile)te).getWaypoints().get(mes.index);
					tile.setDestination(coord);
					if(coord.getFacing() != null)
						tile.setExteriorFacingDirection(coord.getFacing());
				}
			});
		});
		cont.get().setPacketHandled(true);
	}
}
