package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.tileentities.machines.QuantiscopeTile;
import net.tardis.mod.tileentities.machines.QuantiscopeTile.EnumMode;

public class QuantiscopeTabMessage {
	
	private BlockPos pos;
	private EnumMode mode;
	
	public QuantiscopeTabMessage(BlockPos pos, EnumMode mode) {
		this.pos = pos;
		this.mode = mode;
	}
	
	public static QuantiscopeTabMessage decode(PacketBuffer buf) {
		return new QuantiscopeTabMessage(buf.readBlockPos(), EnumMode.values()[buf.readInt()]);
	}
	
	public static void encode(QuantiscopeTabMessage mes, PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
		buf.writeInt(mes.mode.ordinal());
	}
	
	public static void handle(QuantiscopeTabMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			TileEntity te = cont.get().getSender().world.getTileEntity(mes.pos);
			if(te instanceof QuantiscopeTile) {
				((QuantiscopeTile)te).setMode(mes.mode);
				NetworkHooks.openGui(cont.get().getSender(), new INamedContainerProvider() {

					@Override
					public Container createMenu(int id, PlayerInventory inv, PlayerEntity entity) {
						return ((QuantiscopeTile)te).createContainer(id, inv);
					}

					@Override
					public ITextComponent getDisplayName() {
						return new StringTextComponent("");
					}
					
				}, mes.pos);
			}
		});
		cont.get().setPacketHandled(true);
	}

}
