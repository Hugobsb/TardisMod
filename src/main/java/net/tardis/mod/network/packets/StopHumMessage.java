package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.sounds.InteriorHum;

public class StopHumMessage{

    private InteriorHum hum;

    public StopHumMessage() {
    }

    public StopHumMessage(InteriorHum hum) {
        this.hum = hum;
    }

    public static void encode(StopHumMessage mes, PacketBuffer buf) {
    	buf.writeRegistryId(mes.hum);
    }

    public static StopHumMessage decode(PacketBuffer buf) {
        return new StopHumMessage(buf.readRegistryId());
    }

    public static void handle(StopHumMessage mes,  Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(()->{
            InteriorHum humToStop = mes.hum;
            if (humToStop != null) { //Must have this null check because readRegistryId will throw an exception if we use the disabled hum, which has no sound event
                if (humToStop.getLoopedSoundEvent() != null)
            	    Minecraft.getInstance().getSoundHandler().stop(humToStop.getLoopedSoundEvent().getName(), SoundCategory.AMBIENT);
            }
        });
        ctx.get().setPacketHandled(true);
    }
}

