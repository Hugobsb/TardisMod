package net.tardis.mod.client.models.interiordoors;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.TRenderTypes;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.entity.DoorRenderer;
import net.tardis.mod.client.renderers.exteriors.TT2020CapsuleExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;

public class TT2020InteriorModel extends AbstractInteriorDoorModel{
    private final ModelRenderer door;
    private final ModelRenderer door_west_rotate_y;
    private final ModelRenderer door_dot_west_a;
    private final ModelRenderer door_dot_west_b;
    private final ModelRenderer door_dot_west_c;
    private final ModelRenderer door_dot_west_d;
    private final ModelRenderer door_dot_west_e;
    private final ModelRenderer door_east_rotate_y;
    private final ModelRenderer door_dot_east_a;
    private final ModelRenderer door_dot_east_b;
    private final ModelRenderer door_dot_east_c;
    private final ModelRenderer door_dot_east_d;
    private final ModelRenderer door_dot_east_e;
    private final ModelRenderer door_jam;
    private final ModelRenderer door_outline;
    private final ModelRenderer boti;

    public TT2020InteriorModel() {
        textureWidth = 512;
        textureHeight = 512;

        door = new ModelRenderer(this);
        door.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        door_west_rotate_y = new ModelRenderer(this);
        door_west_rotate_y.setRotationPoint(-60.0F, -72.0F, -30.0F);
        door.addChild(door_west_rotate_y);
        door_west_rotate_y.setTextureOffset(397, 249).addBox(36.0F, 25.0F, 19.0F, 24.0F, 44.0F, 4.0F, 0.0F, false);
        door_west_rotate_y.setTextureOffset(397, 206).addBox(36.0F, -19.0F, 19.0F, 24.0F, 44.0F, 4.0F, 0.0F, false);
        door_west_rotate_y.setTextureOffset(397, 163).addBox(36.0F, -63.0F, 19.0F, 24.0F, 44.0F, 4.0F, 0.0F, false);
        door_west_rotate_y.setTextureOffset(56, 105).addBox(38.0F, -61.0F, 23.0F, 20.0F, 42.0F, 4.0F, 0.0F, false);
        door_west_rotate_y.setTextureOffset(56, 105).addBox(38.0F, 25.0F, 23.0F, 20.0F, 42.0F, 4.0F, 0.0F, false);
        door_west_rotate_y.setTextureOffset(56, 105).addBox(38.0F, -19.0F, 23.0F, 20.0F, 44.0F, 4.0F, 0.0F, false);

        door_dot_west_a = new ModelRenderer(this);
        door_dot_west_a.setRotationPoint(60.0F, 72.0F, 30.0F);
        door_west_rotate_y.addChild(door_dot_west_a);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-18.0F, -129.0F, -10.0F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-17.0F, -130.0F, -10.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-19.0F, -128.0F, -10.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-6.0F, -128.0F, -10.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-5.0F, -126.0F, -10.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-20.0F, -126.0F, -10.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-17.0F, -117.0F, -10.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-15.0F, -116.0F, -10.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-15.0F, -131.0F, -10.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_west_b = new ModelRenderer(this);
        door_dot_west_b.setRotationPoint(60.0F, 72.0F, 30.0F);
        door_west_rotate_y.addChild(door_dot_west_b);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-18.0F, -102.0F, -10.0F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-17.0F, -103.0F, -10.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-19.0F, -101.0F, -10.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-6.0F, -101.0F, -10.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-5.0F, -99.0F, -10.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-20.0F, -99.0F, -10.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-17.0F, -90.0F, -10.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-15.0F, -89.0F, -10.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-15.0F, -104.0F, -10.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_west_c = new ModelRenderer(this);
        door_dot_west_c.setRotationPoint(60.0F, 72.0F, 30.0F);
        door_west_rotate_y.addChild(door_dot_west_c);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-18.0F, -75.0F, -10.0F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-17.0F, -76.0F, -10.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-19.0F, -74.0F, -10.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-6.0F, -74.0F, -10.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-5.0F, -72.0F, -10.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-20.0F, -72.0F, -10.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-17.0F, -63.0F, -10.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-15.0F, -62.0F, -10.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-15.0F, -77.0F, -10.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_west_d = new ModelRenderer(this);
        door_dot_west_d.setRotationPoint(60.0F, 72.0F, 30.0F);
        door_west_rotate_y.addChild(door_dot_west_d);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-18.0F, -48.0F, -10.0F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-17.0F, -49.0F, -10.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-19.0F, -47.0F, -10.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-6.0F, -47.0F, -10.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-5.0F, -45.0F, -10.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-20.0F, -45.0F, -10.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-17.0F, -36.0F, -10.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-15.0F, -35.0F, -10.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-15.0F, -50.0F, -10.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_west_e = new ModelRenderer(this);
        door_dot_west_e.setRotationPoint(60.0F, 72.0F, 30.0F);
        door_west_rotate_y.addChild(door_dot_west_e);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-18.0F, -21.0F, -10.0F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-17.0F, -22.0F, -10.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-19.0F, -20.0F, -10.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-6.0F, -20.0F, -10.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-5.0F, -18.0F, -10.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-20.0F, -18.0F, -10.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-17.0F, -9.0F, -10.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-15.0F, -8.0F, -10.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-15.0F, -23.0F, -10.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_east_rotate_y = new ModelRenderer(this);
        door_east_rotate_y.setRotationPoint(60.0F, -72.0F, -30.0F);
        door.addChild(door_east_rotate_y);
        door_east_rotate_y.setTextureOffset(422, 249).addBox(-60.0F, 25.0F, 19.0F, 24.0F, 44.0F, 4.0F, 0.0F, false);
        door_east_rotate_y.setTextureOffset(419, 207).addBox(-60.0F, -19.0F, 19.0F, 24.0F, 44.0F, 4.0F, 0.0F, false);
        door_east_rotate_y.setTextureOffset(422, 163).addBox(-60.0F, -63.0F, 19.0F, 24.0F, 44.0F, 4.0F, 0.0F, false);
        door_east_rotate_y.setTextureOffset(56, 105).addBox(-58.0F, -61.0F, 23.0F, 20.0F, 42.0F, 4.0F, 0.0F, false);
        door_east_rotate_y.setTextureOffset(56, 105).addBox(-58.0F, 25.0F, 23.0F, 20.0F, 42.0F, 4.0F, 0.0F, false);
        door_east_rotate_y.setTextureOffset(56, 105).addBox(-58.0F, -19.0F, 23.0F, 20.0F, 44.0F, 4.0F, 0.0F, false);

        door_dot_east_a = new ModelRenderer(this);
        door_dot_east_a.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
        door_east_rotate_y.addChild(door_dot_east_a);
        door_dot_east_a.setTextureOffset(288, 98).addBox(5.9734F, -129.0F, -10.6101F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(6.9734F, -130.0F, -10.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(4.9734F, -128.0F, -10.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(17.9734F, -128.0F, -10.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(18.9734F, -126.0F, -10.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(3.9734F, -126.0F, -10.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(6.9734F, -117.0F, -10.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(8.9734F, -116.0F, -10.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(8.9734F, -131.0F, -10.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_east_b = new ModelRenderer(this);
        door_dot_east_b.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
        door_east_rotate_y.addChild(door_dot_east_b);
        door_dot_east_b.setTextureOffset(288, 98).addBox(5.9734F, -102.0F, -10.6101F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(6.9734F, -103.0F, -10.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(4.9734F, -101.0F, -10.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(17.9734F, -101.0F, -10.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(18.9734F, -99.0F, -10.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(3.9734F, -99.0F, -10.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(6.9734F, -90.0F, -10.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(8.9734F, -89.0F, -10.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(8.9734F, -104.0F, -10.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_east_c = new ModelRenderer(this);
        door_dot_east_c.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
        door_east_rotate_y.addChild(door_dot_east_c);
        door_dot_east_c.setTextureOffset(288, 98).addBox(5.9734F, -75.0F, -10.6101F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(6.9734F, -76.0F, -10.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(4.9734F, -74.0F, -10.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(17.9734F, -74.0F, -10.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(18.9734F, -72.0F, -10.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(3.9734F, -72.0F, -10.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(6.9734F, -63.0F, -10.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(8.9734F, -62.0F, -10.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(8.9734F, -77.0F, -10.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_east_d = new ModelRenderer(this);
        door_dot_east_d.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
        door_east_rotate_y.addChild(door_dot_east_d);
        door_dot_east_d.setTextureOffset(288, 98).addBox(5.9734F, -48.0F, -10.6101F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(6.9734F, -49.0F, -10.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(4.9734F, -47.0F, -10.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(17.9734F, -47.0F, -10.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(18.9734F, -45.0F, -10.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(3.9734F, -45.0F, -10.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(6.9734F, -36.0F, -10.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(8.9734F, -35.0F, -10.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(8.9734F, -50.0F, -10.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_east_e = new ModelRenderer(this);
        door_dot_east_e.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
        door_east_rotate_y.addChild(door_dot_east_e);
        door_dot_east_e.setTextureOffset(288, 98).addBox(5.9734F, -21.0F, -10.6101F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(6.9734F, -22.0F, -10.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(4.9734F, -20.0F, -10.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(17.9734F, -20.0F, -10.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(18.9734F, -18.0F, -10.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(3.9734F, -18.0F, -10.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(6.9734F, -9.0F, -10.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(8.9734F, -8.0F, -10.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(8.9734F, -23.0F, -10.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_jam = new ModelRenderer(this);
        door_jam.setRotationPoint(0.0F, 24.0F, 0.0F);
        door_jam.setTextureOffset(64, 57).addBox(22.0F, -68.0F, -6.0F, 4.0F, 64.0F, 14.0F, 0.0F, false);
        door_jam.setTextureOffset(64, 57).addBox(22.0F, -132.0F, -6.0F, 4.0F, 64.0F, 14.0F, 0.0F, false);
        door_jam.setTextureOffset(64, 57).addBox(-26.0F, -68.0F, -6.0F, 4.0F, 64.0F, 14.0F, 0.0F, false);
        door_jam.setTextureOffset(64, 57).addBox(-26.0F, -132.0F, -6.0F, 4.0F, 64.0F, 14.0F, 0.0F, false);
        door_jam.setTextureOffset(34, 116).addBox(0.0F, -140.0F, -6.0F, 26.0F, 8.0F, 14.0F, 0.0F, false);
        door_jam.setTextureOffset(34, 116).addBox(-26.0F, -140.0F, -6.0F, 26.0F, 8.0F, 14.0F, 0.0F, false);
        door_jam.setTextureOffset(34, 76).addBox(0.0F, -4.0F, -8.0F, 26.0F, 4.0F, 16.0F, 0.0F, false);
        door_jam.setTextureOffset(39, 103).addBox(-26.0F, -4.0F, -8.0F, 26.0F, 4.0F, 16.0F, 0.0F, false);

        door_outline = new ModelRenderer(this);
        door_outline.setRotationPoint(0.0F, 24.0F, 0.0F);
        door_outline.setTextureOffset(22, 263).addBox(26.0F, -68.0F, -12.0F, 1.0F, 68.0F, 8.0F, 0.0F, false);
        door_outline.setTextureOffset(22, 263).addBox(-27.0F, -68.0F, -12.0F, 1.0F, 68.0F, 8.0F, 0.0F, false);
        door_outline.setTextureOffset(22, 263).addBox(26.0F, -136.0F, -12.0F, 1.0F, 68.0F, 8.0F, 0.0F, false);
        door_outline.setTextureOffset(22, 263).addBox(-27.0F, -136.0F, -12.0F, 1.0F, 68.0F, 8.0F, 0.0F, false);
        door_outline.setTextureOffset(22, 263).addBox(-26.0F, -136.0F, -11.0F, 52.0F, 1.0F, 7.0F, 0.0F, false);
        door_outline.setTextureOffset(22, 263).addBox(-26.0F, -1.0F, -11.0F, 52.0F, 1.0F, 3.0F, 0.0F, false);

        boti = new ModelRenderer(this);
        boti.setRotationPoint(0.0F, 24.0F, 0.0F);
        boti.setTextureOffset(22, 263).addBox(-26.0F, -76.0F, -12.0F, 52.0F, 12.0F, 1.0F, 0.0F, false);
        boti.setTextureOffset(22, 263).addBox(-26.0F, -64.0F, -12.0F, 52.0F, 64.0F, 1.0F, 0.0F, false);
        boti.setTextureOffset(22, 263).addBox(-26.0F, -136.0F, -12.0F, 52.0F, 60.0F, 1.0F, 0.0F, false);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void renderBones(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {
        matrixStack.push();
        matrixStack.translate(0, 1.125, -0.4);
        matrixStack.scale(0.25F, 0.25F, 0.25F);
        float rot = (float) Math.toRadians(360 - EnumDoorType.TT_2020_CAPSULE.getRotationForState(door.getOpenState()));
        this.door_west_rotate_y.rotateAngleY = rot;
        this.door_east_rotate_y.rotateAngleY = -rot;
        this.renderDoorWhenClosed(door, matrixStack, buffer, packedLight, packedOverlay, this.door);
        this.door_jam.render(matrixStack, buffer, packedLight, packedOverlay);
        this.door_outline.render(matrixStack, buffer, packedLight, packedOverlay);
        matrixStack.pop();
    }
    
    
    @Override
    public void renderBoti(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay) {
        if(Minecraft.getInstance().world != null && door.getOpenState() != EnumDoorState.CLOSED){
            Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
                PortalInfo info = new PortalInfo();
                info.setWorldShell(data.getBotiWorld());
                info.setPosition(door.getPositionVec());

                //Translations
                info.setTranslate(matrix -> {
                    DoorRenderer.applyTranslations(matrix, door.rotationYaw - 180, door.getHorizontalFacing());
                    matrix.translate(0, 1.15, -0.75);
                    matrix.rotate(Vector3f.ZN.rotationDegrees(180));
                });
                info.setTranslatePortal((matrix) -> {
                    matrix.translate(0, -0.5, 0);
                    matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(data.getBotiWorld().getPortalDirection())));
                    TT2020CapsuleExteriorRenderer.translateFloating(matrix);
                });

                //Renderers

                info.setRenderPortal((matrix, buf) -> {
                    matrix.push();
                    matrix.scale(-0.25F, -0.25F, -0.25F);
                    this.boti.render(matrix, buf.getBuffer(TRenderTypes.getTardis(this.getTexture())), packedLight, packedOverlay);
                    matrix.pop();
                });
                info.setRenderDoor((matrix, buf) -> {
                    matrix.push();
                    matrix.rotate(Vector3f.ZP.rotationDegrees(180));
                    matrix.scale(0.25F, 0.25F, 0.25F);
                    TT2020CapsuleExteriorRenderer.translateFloating(matrix);
                    matrix.translate(0, 0, 0.75F);
                    this.door.render(matrix, buf.getBuffer(RenderType.getEntityCutout(getTexture())), packedLight, packedOverlay);
                    matrix.pop();
                });
                BOTIRenderer.addPortal(info);
            });
        }
    }

    @Override
    public ResourceLocation getTexture() {
        return TT2020CapsuleExteriorRenderer.TEXTURE;
    }

    @Override
    public boolean doesDoorOpenIntoBotiWindow() {
        return true;
    }

}