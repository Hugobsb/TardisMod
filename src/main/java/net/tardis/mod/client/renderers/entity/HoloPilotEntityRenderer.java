package net.tardis.mod.client.renderers.entity;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.entity.HoloPilotEntity;

public class HoloPilotEntityRenderer extends LivingRenderer<HoloPilotEntity, PlayerModel<HoloPilotEntity>>{

	public HoloPilotEntityRenderer(EntityRendererManager rendererManager) {
		super(rendererManager, new PlayerModel<HoloPilotEntity>(0.0625F, true), 0.3F);
	}

	@Override
	public ResourceLocation getEntityTexture(HoloPilotEntity entity) {
		return new ResourceLocation("textures/entity/steve.png");
	}

	@Override
	public void render(HoloPilotEntity entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn,
	        IRenderTypeBuffer bufferIn, int packedLightIn) {
		matrixStackIn.push();
		RenderSystem.enableBlend();
		RenderSystem.color4f(0.7F, 0.7F, 1F, 0.5F);
		super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
		RenderSystem.disableBlend();
		matrixStackIn.pop();
	}
	
	

}
