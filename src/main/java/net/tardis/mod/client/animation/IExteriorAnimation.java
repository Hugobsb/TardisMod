package net.tardis.mod.client.animation;

import net.minecraft.nbt.IntNBT;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public interface IExteriorAnimation extends ITardisAnimation<IntNBT> {

	void tick(int timeLeft);
	ExteriorAnimationEntry getType();
	float getAlpha();
	/** Start the animation timer. 
	 * */
	void startAnim(EnumMatterState state, int timeToDemat);

    //resets any internal variables this may hold
    default void reset() {}

    interface IAnimSpawn<T extends ExteriorAnimation> {
        T create(ExteriorAnimationEntry entry, ExteriorTile tile);
    }

}
