package net.tardis.mod.client.guis.radial;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TranslationTextComponent;

/** Implementation of IRadialAction which renders an ItemStack in the center of the button to act as the icon*/
public class ItemRadialAction implements IRadialAction{

    private ItemStack renderStack;
    private TranslationTextComponent translationTextComponent;
    private Runnable action;

    public ItemRadialAction(ItemStack stack, Runnable action){
        this.renderStack = stack;
        this.action = action;
        this.translationTextComponent = new TranslationTextComponent("");
    }

    public ItemRadialAction(ItemStack renderStack, TranslationTextComponent translationTextComponent, Runnable action) {
        this.renderStack = renderStack;
        this.translationTextComponent = translationTextComponent;
        this.action = action;
    }


    @Override
    public void render(MatrixStack matrix, int x, int y, float partialTicks) {
        Minecraft.getInstance().getItemRenderer().renderItemIntoGUI(this.renderStack, x + (34 / 2 - 8), y + (37 / 2 - 8));
    }


    @Override
    public void run() {
        this.action.run();
    }

    @Override
    public TranslationTextComponent getTextComponent() {
        return translationTextComponent;
    }
}
