package net.tardis.mod.client.guis.widgets;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.client.guis.radial.IRadialAction;
import net.tardis.mod.constants.TardisConstants;

public class RadialButton extends Button {

    private ResourceLocation texture;
    private IRadialAction action;

    public RadialButton(int x, int y, ResourceLocation texture, IRadialAction action) {
        super(x - 34 / 2, y - 37 / 2, 34, 37, new StringTextComponent(""), but -> action.run());
        this.action = action;
        this.texture = texture;
    }

    @Override
    public void renderWidget(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        Minecraft.getInstance().getTextureManager().bindTexture(this.texture);
        this.blit(matrixStack, this.x, this.y, 143, 112, this.width, this.height);
        this.action.render(matrixStack, this.x, this.y, partialTicks);
    }

    public IRadialAction getAction() {
        return action;
    }
}
